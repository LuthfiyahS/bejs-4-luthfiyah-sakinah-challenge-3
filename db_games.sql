--CREATE DATABASE
CREATE DATABASE db_games_challenge
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;


--CREATE TABLE 
CREATE TABLE IF NOT EXISTS public.games_level
(
    level_id serial NOT NULL,
    name character varying(50) NOT NULL,
    difficultly character varying(50) NOT NULL,
    achieved_score integer NOT NULL,
    PRIMARY KEY (level_id)
);

CREATE TABLE IF NOT EXISTS public.user_games
(
    user_id serial NOT NULL,
    username character varying(25) NOT NULL,
    password character varying(25) NOT NULL,
    email character varying(255) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    PRIMARY KEY (user_id)
);


CREATE TABLE IF NOT EXISTS public.user_games_biodata
(
    biodata_user_id serial NOT NULL,
    user_id integer NOT NULL,
    level_id integer NOT NULL,
    score integer NOT NULL,
    nickname character varying(25) NOT NULL,
    gender character varying(15) NULL,
    updated_at time with time zone NOT NULL,
    PRIMARY KEY (biodata_user_id),
    CONSTRAINT user_id_fk FOREIGN KEY (user_id)
        REFERENCES public.user_games (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
        NOT VALID,
    CONSTRAINT level_id_fk FOREIGN KEY (level_id)
    REFERENCES public.games_level (level_id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION
    NOT VALID
);


CREATE TABLE IF NOT EXISTS public.user_games_history
(
    history_id serial NOT NULL,
    user_id integer NOT NULL,
    obtained_score integer NOT NULL,
    session_start timestamp with time zone NOT NULL,
    session_end timestamp with time zone NOT NULL,
    stamp timestamp with time zone NOT NULL,
    PRIMARY KEY (history_id),
    CONSTRAINT user_id_fk FOREIGN KEY (user_id)
        REFERENCES public.user_games (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE CASCADE
        NOT VALID
);

--TRIGGER biar insert or delete biodata otomatis dari user_games
CREATE OR REPLACE FUNCTION process_user_bio() RETURNS TRIGGER AS $biodata$
    BEGIN
        INSERT INTO user_games_biodata(biodata_user_id,user_id,level_id,nickname,score,updated_at) VALUES(NEW.user_id ,NEW.user_id , 1,NEW.username , 0 , current_timestamp) ;
        RETURN NEW;
        RETURN NULL; 
    END;
$biodata$ LANGUAGE plpgsql;

CREATE TRIGGER user_games_biodata
AFTER INSERT ON public.user_games
    FOR EACH ROW EXECUTE PROCEDURE process_user_bio();

--TRIGGER agar level dan score yang ada biodata terus terupdate saat kita menginputkan history permainan
CREATE OR REPLACE FUNCTION process_score_bio() RETURNS TRIGGER AS $updatescore$
    BEGIN
        UPDATE user_games_biodata SET level_id=(((score+ NEW.obtained_score)/50)+1), score = score+ NEW.obtained_score WHERE biodata_user_id = NEW.user_id;
		IF NOT FOUND THEN RETURN NULL; END IF;
        RETURN NEW;
    END;
$updatescore$ LANGUAGE plpgsql;

CREATE TRIGGER user_games_biodata
AFTER INSERT ON public.user_games_history
    FOR EACH ROW EXECUTE PROCEDURE process_score_bio();


--INSERT games_level
INSERT INTO public.games_level(
	name, difficultly, achieved_score)
	VALUES 
	('Warrior', 'Basic', 50),
	('Elite', 'Basic', 100),
	('GrandMaster', 'Intermediate', 150),
	('Epic', 'Intermediate', 200),
	('Legend', 'Hard', 250),
	('Mythic', 'Hard', 300);

--INSERT user_games_biodata udah otomatis keisi biodatanya juga pake trigger
INSERT INTO public.user_games(
	 username, password, email, created_at)
	VALUES 
	('ayyustr','12121212','ayyustr@gmail.com',current_timestamp),
    ('adilaar','12121212','adilaar@gmail.com',current_timestamp),
    ('tono','12121212','tono@gmail.com',current_timestamp),
    ('budi','12121212','budi@gmail.com',current_timestamp),
    ('ani','12121212','ani@gmail.com',current_timestamp),
    ('aul','12121212','aul@gmail.com',current_timestamp),
    ('beni','12121212','beni@gmail.com',current_timestamp),
    ('ando','12121212','ando@gmail.com',current_timestamp),
    ('ega','12121212','ega@gmail.com',current_timestamp),
    ('sita','12121212','sita@gmail.com',current_timestamp);

--INSERT user_games_history akan membuat  level dan score di user_games_biodata terupdate secara otomatis menggunakan trigger
INSERT INTO public.user_games_history(
	user_id, obtained_score, session_start, session_end, stamp)
    VALUES 
	(1,12,current_timestamp,current_timestamp,current_timestamp),
    (2,43,current_timestamp,current_timestamp,current_timestamp),
    (3,5,current_timestamp,current_timestamp,current_timestamp),
    (4,23,current_timestamp,current_timestamp,current_timestamp),
    (4,34,current_timestamp,current_timestamp,current_timestamp),
    (4,23,current_timestamp,current_timestamp,current_timestamp),
    (5,65,current_timestamp,current_timestamp,current_timestamp),
    (6,34,current_timestamp,current_timestamp,current_timestamp),
    (6,55,current_timestamp,current_timestamp,current_timestamp),
    (6,32,current_timestamp,current_timestamp,current_timestamp),
    (7,76,current_timestamp,current_timestamp,current_timestamp),
    (8,54,current_timestamp,current_timestamp,current_timestamp),
    (9,23,current_timestamp,current_timestamp,current_timestamp),
    (10,45,current_timestamp,current_timestamp,current_timestamp);

--SELECT TABEL
SELECT * FROM public.games_level ORDER BY level_id ASC ;
SELECT * FROM public.user_games ORDER BY user_id ASC ;
SELECT * FROM public.user_games_biodata ORDER BY biodata_user_id ASC ;
SELECT * FROM public.user_games_history ORDER BY history_id ASC ;


--SELECT JOIN 4 TABEL
SELECT 
d.history_id,
a.user_id,
a.username,
b.nickname,
b.gender,
c.name as level_name,
d.obtained_score,
b.score as score_now
FROM user_games a
LEFT JOIN user_games_biodata b ON b.user_id = a.user_id
LEFT JOIN games_level c ON c.level_id = b.level_id
LEFT JOIN user_games_history d ON d.user_id = a.user_id
ORDER BY a.user_id ASC;

--SELECT JOIN 3 TABEL
SELECT 
a.user_id,
a.email,
b.nickname,
b.gender,
c.name as level_name,
b.score
FROM user_games a
LEFT JOIN user_games_biodata b ON b.user_id = a.user_id
LEFT JOIN games_level c ON c.level_id = b.level_id
ORDER BY a.user_id ASC;

--SELECT JOIN 2 TABEL
SELECT 
a.user_id, a.email,
b.nickname, b.gender, b.score
FROM user_games a
LEFT JOIN user_games_biodata b ON b.user_id = a.user_id
ORDER BY a.user_id ASC;

SELECT 
b.nickname, b.gender, b.score,
c.name as level_name
FROM user_games_biodata b
LEFT JOIN games_level c ON c.level_id = b.level_id
ORDER BY b.user_id ASC;

SELECT 
c.user_id, c.username,
a.obtained_score
FROM user_games c
LEFT JOIN user_games_history a ON a.user_id = c.user_id
ORDER BY c.user_id ASC;

--UPDATE DATA
UPDATE public.games_level SET name='Junior' WHERE level_id=1;
UPDATE public.user_games SET username='adilaar' WHERE user_id=2;
UPDATE public.user_games_biodata SET gender='FEMALE' WHERE biodata_user_id<=2;
UPDATE public.user_games_biodata SET gender='MALE' WHERE biodata_user_id=3;
UPDATE public.user_games_biodata SET gender='MALE' WHERE biodata_user_id=9;
UPDATE public.user_games_biodata SET gender='FEMALE' WHERE biodata_user_id=10;
UPDATE public.user_games_history SET obtained_score=0 WHERE history_id=3;


--DELETE DATA
DELETE FROM public.games_level WHERE level_id=6;
DELETE FROM public.user_games WHERE user_id=1; 
DELETE FROM public.user_games_biodata WHERE biodata_user_id=6; 
DELETE FROM public.user_games_history WHERE history_id=1;